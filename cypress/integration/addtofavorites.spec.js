describe('Adds and removes contact to favorites', () => {
  it('Adds 5th contact to favorites after click', () => {
    cy.visit('/');
    cy.get('.favorite-btn')
      .eq(4).click();
    cy.get('.favorites')
      .should($favorite => {
        expect($favorite).to.contain('Willard Hudson');
      });
    });
    it('Should show "Unfavorize-button" on ContactCard', () => {
      expect(cy.get('.all')
        .children('.friend')
        .eq(4).contains('Unfavorize'));
  });
    it('Should remove favorite on click on unfavorize', () => {
      cy.get('.favorites')
        .children('.friend')
        .eq(0).find('.unfavorize')
        .click();
      expect(cy.get('.favorites').should('not.be.visible'));
    })
});