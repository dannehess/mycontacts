describe('Add new contact', () => {
  it('Renders form after click on button', () => {
    cy.visit('/');
    cy.get('.create-contact-forms').should('not.be.visible');
    cy.get('.addcontact-btn').click();
    cy.get('.create-contact-forms').should('be.visible');
  });
  it('Should disable the form', () => {
    cy.get('.close').click();
    cy.get('.create-contact-forms').should('not.be.visible');
  });
  it('Should fill the form', () => {
    cy.get('.addcontact-btn').click();
    cy.get('input[name="image"]')
      .type('https://danielhessling.com/assets/images/avatar.png')
      .should('have.value', 'https://danielhessling.com/assets/images/avatar.png');
    cy.get('input[name="fullName"]')
      .type('Daniel Hessling')
      .should('have.value', 'Daniel Hessling');
    cy.get('input[name="email"]')
      .type('daniel.hessling@gmail.com')
      .should('have.value', 'daniel.hessling@gmail.com');
    cy.get('input[name="phone"]')
      .type('0704943141')
      .should('have.value', '0704943141');
    cy.get('input[name="street"]')
      .type('Fakestreet 22')
      .should('have.value', 'Fakestreet 22');
    cy.get('input[name="zipcode"]')
      .type('45322')
      .should('have.value', '45322');
    cy.get('input[name="city"]')
      .type('Faketown')
      .should('have.value', 'Faketown');
    });
  it('Should render the new contact and disable form', () => {
    cy.get('.add-btn').click();
    cy.get('.friend')
      .should($friend => {
        expect($friend).to.have.length(7);
        expect($friend.first()).to.contain('Daniel Hessling');
      })
    cy.get('.create-contact-forms').should('not.be.visible');
  });
});