describe('It should render, delete and update contacts', () => {
  it('Should render 6 contacts from start', () => {
    cy.visit('/');
    cy.get('.all')
      .find('.friend').should('have.length', 6);
  })
  it('Should find Willard Hudson (fourth contact)', () => {
    cy.get('.all')
      .should($contact => {
        expect($contact).to.contain('Willard Hudson');
      })
  })
  it('Should delete fourth contact', () => {
    cy.get('.remove').eq(4).click();
    cy.get('.all')
    .should($contact => {
      expect($contact).not.to.contain('Willard Hudson');
    })
  })
  it('Should render 5 contacts', () => {
    cy.get('.all')
      .find('.friend').should('have.length', 5);
  })
});