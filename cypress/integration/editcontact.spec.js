describe('Edit contact', function () {
  it('Opens and fills the third contacts info in the form', function () {
    cy.visit('/');
    cy.get('.edit')
      .eq(2).click();
    cy.get('.create-contact-forms').should('be.visible');
    cy.get('input[name="image"]')
      .should('have.value', 'https://images.pexels.com/photos/34776/homeless-male-b-w-person.jpg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940');
    cy.get('input[name="fullName"]')
      .should('have.value', 'Damon Graves');
    cy.get('input[name="email"]')
      .should('have.value', 'damon.graves@abc123.com');
    cy.get('input[name="phone"]')
      .should('have.value', '070-999 88 7X');
    cy.get('input[name="street"]')
      .should('have.value', 'Fakestreet 22');
    cy.get('input[name="zipcode"]')
      .should('have.value', '234 09');
    cy.get('input[name="city"]')
      .should('have.value', 'Faketown');
  });
  it('Should update last name and phonenumber', () => {
    cy.get('input[name="fullName"]').clear()
    .type('Damon Phillips')
    .should('have.value', 'Damon Phillips');
    cy.get('input[name="phone"]').clear()
      .type('073-999-88-3X')
      .should('have.value', '073-999-88-3X');
  });
  it('Should rerender changes and collapse the form', () => {
    cy.get('.save').click();
    cy.get('.create-contact-forms').should('not.be.visible');
    cy.get('.friend')
      .should($friend => {
        expect($friend).to.contain('Damon Phillips');
      })
  })
});