import React from 'react';
import logo from '../images/logo.png';

export default class Logo extends React.Component {
  render() {
    return (
      <>
        <span className="logo">
          <img src={logo} alt="logo" className="logoimg" />
          My Contacts</span>
      </>
    )
  }
}