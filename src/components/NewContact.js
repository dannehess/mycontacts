import React from 'react';

export default class NewContact extends React.Component {

  state = {
    showing: false,
    isValid: false,
    image: '',
    fullName: '',
    email: '',
    phone: '',
    street: '',
    zipcode: '',
    city: ''
  }

  toggleVisibility() {
    this.setState({
      showing: !this.state.showing
    })
  }

  handleChange = e => {
    this.setState({
      [e.target.name]: e.target.value,
    })
  }

  clearForm = () => {
    this.setState({
      showing: false,
      image: '',
      fullName: '',
      email: '',
      phone: '',
      street: '',
      zipcode: '',
      city: ''
    })
  }

  onSubmit = e => {
    e.preventDefault();
    const contact = {
      id: this.props.checkHighestId() + 1,
      image: this.state.image,
      fullName: this.state.fullName,
      email: this.state.email,
      phone: this.state.phone,
      street: this.state.street,
      zipcode: this.state.zipcode,
      city: this.state.city
    }

    if(contact.fullName !== ''){
      this.props.addContact(contact);
      this.clearForm();
    }
    else{
      alert('Enter a name');
    }
  }

  render() {
    const { showing } = this.state;
    return (
      <>
        {!this.state.showing ?
          <div>
            <div className="spacer"></div>
            <button
              className="addcontact-btn"
              onClick={() => this.toggleVisibility()}>
              Add new contact</button>
          </div>
          : null}
        {showing ?
          <div className={showing ? 'show' : 'd-none'}>
            <div className="spacer"></div>
            <form className="create-contact-forms" data-test="addcontact">
              <div className="add-contact">
                <div className="top">
                  <h1 className="heading">Add Contact</h1>
                  <img
                    className="close"
                    onClick={this.clearForm}
                    alt="delete"
                    src={require('../images/close.png')} />
                </div>
                <div className="imgurl">
                  <label>Image URL:</label>
                  <input
                    type="text"
                    name="image"
                    value={this.state.image}
                    onChange={this.handleChange}
                  />
                </div>
                <div className="rows">
                  <div className="row">
                    <label>Name:</label>
                    <input
                      type="text"
                      name="fullName"
                      value={this.state.fullName}
                      onChange={this.handleChange}
                    />
                    <label>Email:</label>
                    <input
                      type="email"
                      name="email"
                      value={this.state.email}
                      onChange={this.handleChange}
                    />
                    <label>Phone:</label>
                    <input
                      type="text"
                      name="phone"
                      value={this.state.phone}
                      onChange={this.handleChange}
                    />
                  </div>
                  <div className="row">
                    <label>Street:</label>
                    <input
                      type="text"
                      name="street"
                      value={this.state.street}
                      onChange={this.handleChange}
                    />
                    <label>Zipcode:</label>
                    <input
                      type="text"
                      name="zipcode"
                      value={this.state.zipcode}
                      onChange={this.handleChange}
                    />
                    <label>City:</label>
                    <input
                      type="text"
                      name="city"
                      value={this.state.city}
                      onChange={this.handleChange}
                      required
                    />
                  </div>
                </div>
                <div className="add-actions">
                  <button
                    className="delete"
                    onClick={this.clearForm}>Clear form</button>
                  <button
                    className="add-btn"
                    type="submit"
                    onClick={this.onSubmit}>Add Contact</button>
                </div>
              </div>
            </form>
          </div>
          : null}
      </>
    )
  }
}