import React from 'react';
import ContactCard from './ContactCard';
import NewContact from './NewContact';
import contactsData from '../contacts.json';
import EditContact from './EditContact';
import Favorites from './Favorites';

export default class Friends extends React.Component {

  state = {
    contacts: contactsData,
    isInEditMode: false,
    favorites: []
  }
  
  componentDidMount() {
    this.setState({
      contacts: contactsData
    })
  }

  addContact = newContact => {
    this.setState({ contacts: [newContact, ...this.state.contacts]});
  }

  editContact = contact => {
    const userID = this.state.userInEdit.map(item => {
      return item.id
    })
    const updatedContacts = this.state.contacts.filter(item => {
      return item.id !== userID[0]
    })
    this.setState({
      contacts: [contact, ...updatedContacts]
    })
  }

  removeContact = id => {
    const filteredContacts = this.state.contacts.filter(item => {
      return item.id !== id && item;
    });
    this.setState({
      contacts: filteredContacts,
    })
  }

  addFavorite = favorite => {
    const { favorites } = this.state;
    if(!favorites.some(alreadyFavorite => alreadyFavorite.id === favorite.id)) {
      this.setState({
        favorites: [...this.state.favorites, favorite],
      })
    }
  }

  removeFavorite = id => {
    const filteredFavorites = this.state.favorites.filter(item => {
      return item.id !== id && item;
    });
    this.setState({
      favorites: filteredFavorites,
    })
  }

  checkHighestId = () => {
    return Math.max(...this.state.contacts.map(o => o.id), 0);
  }
  
  toggleEditMode = id => {
    this.setState({
      isInEditMode: true,
      userInEdit: this.state.contacts.filter(item => {
        return item.id === id && item
      })
    })
    
  }

  render() {
    return (
      <>
      {this.state.favorites.length !== 0 ?
      <Favorites 
        contacts={this.state.contacts} 
        favorites={this.state.favorites}
        addFavorite={this.addFavorite}
        removeFavorite={this.removeFavorite}
        /> 
        : null}
        <NewContact
          addContact={this.addContact}
          checkHighestId={this.checkHighestId}
          contacts={this.props.contacts}
        />
        {this.state.isInEditMode &&
          <EditContact
            isInEditMode={this.state.isInEditMode}
            userInEdit={this.state.userInEdit}
            addContact={this.addContact}
            editContact={this.editContact}
            removeContact={this.removeContact}
            contacts={this.state.contacts}
            checkHighestId={this.checkHighestId}
             />
        }
        <div className="spacer"></div>
        <div className="friends all">
          {this.state.contacts.map((contact, index) => (
            <ContactCard
              contact={contact}
              key={index}
              removeContact={this.removeContact}
              toggleEditMode={this.toggleEditMode}
              addFavorite={this.addFavorite}
              removeFavorite={this.removeFavorite}
              isFavorite={this.state.favorites.find(pos => pos.id === contact.id)}
            />
          ))}
        </div>
      </>
    );
  }
}