import React from 'react';

export default class Favorite extends React.Component {

  render() {  
  const { favorite, removeFavorite } = this.props;
  return (
    <>
        <div className="friend" key={favorite.id}>
        <div className="friend_img"><img src={favorite.image} alt={favorite.fullName} /></div>
        <div className="desc">
          <h3>{favorite.fullName}</h3>
          <p>{favorite.email}</p>
          <p>{favorite.phone}</p>
          <p>{favorite.street}</p>
          <div>
            <span>{favorite.zipcode} </span>
            <span>{favorite.city}</span>
          </div>
          <div className="actions">
          <button className="delete unfavorize" onClick={() => removeFavorite(favorite.id)}><img alt="delete" className="icon" src={require('../images/favorize.png')} />Unfavorize</button>
        </div>
        </div>
        </div>
    </>
    )
  }
}