import React from 'react';

export default class EditContact extends React.Component{

  state = {
      id: '', 
      image: '',
      fullName: '',
      email: '',
      phone: '',
      street: '',
      zipcode: '',
      city: '',
      contacts: [...this.props.contacts],
      isInEditMode: false,
    }

    componentDidMount() {
      this.updateUser();
    }

    handleChange = e => {
      this.setState({
        [e.target.name]: e.target.value
      })
    }

onSubmit = e => {
  e.preventDefault();

  const contact = {
    id: this.state.id,
    image: this.state.image,
    fullName: this.state.fullName,
    email: this.state.email,
    phone: this.state.phone,
    street: this.state.street,
    zipcode: this.state.zipcode,
    city: this.state.city
  }

      this.props.editContact(contact);
      
      this.setState({
        isInEditMode: false,
      })
    }

    updateUser = () => {
      this.props.userInEdit.map(item => {
        this.setState({
          id: item.id,
          image: item.image,
          fullName: item.fullName,
          email: item.email,
          phone: item.phone,
          street: item.street,
          zipcode: item.zipcode,
          city: item.city,
          isInEditMode: !this.isInEditMode
        })
        return item
      })
    }

    componentDidUpdate(prevProps) {
      prevProps.userInEdit !== this.props.userInEdit && this.updateUser();
    }
  
    render(){
      const { isInEditMode } = this.state;
        return(
            <>
            {isInEditMode ?
            <div className={isInEditMode ? 'show' : 'd-none'}>
            <div className="spacer"></div>
        <form className="create-contact-forms">
          <div className="add-contact">
    <h1 className="heading">Editing {this.state.fullName}</h1>
            <div className="imgurl">
            <label>Image URL:</label>
            <input type="text" name="image" value={this.state.image} onChange={this.handleChange} />
            </div>
              <div className="rows">
              <div className="row">
              <label>Name:</label>
            <input type="text" name="fullName" value={this.state.fullName} onChange={this.handleChange} />
              <label>Email:</label>
            <input type="email" name="email" value={this.state.email} onChange={this.handleChange} />
              <label>Phone:</label>
            <input type="text" name="phone" value={this.state.phone} onChange={this.handleChange} />
            </div>
              <div className="row">
              <label>Street:</label>
            <input type="text" name="street" value={this.state.street} onChange={this.handleChange} />
              <label>Zipcode:</label>
            <input type="text" name="zipcode" value={this.state.zipcode} onChange={this.handleChange} />
              <label>City:</label>
            <input type="text" name="city" value={this.state.city} onChange={this.handleChange} />
            </div>
          </div>
          <div className="add-actions">
            <button className="delete"><img alt="delete" className="icon" src={require('../images/delete.png')} />Clear form</button>
            <button className="btn save" type="submit" onClick={this.onSubmit}><img alt="delete" className="icon" src={require('../images/edit.png')} />Save Contact</button>
            </div>
          </div>
        </form>
        </div>
        : null }
            </>
        )
    }
}