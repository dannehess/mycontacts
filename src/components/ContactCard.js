import React from 'react';

const ContactCard = (props) => {

  const { removeContact,
          toggleEditMode,
          addFavorite,
          removeFavorite,
          isFavorite,
          contact
            } = props;
          
  const {
    index,
    id,
    image,
    fullName,
    email,
    phone,
    street,
    zipcode,
    city,
    } = contact;

          return(
            
            <>
    
        <div className="friend" key={index}>
          <div className="friend_img"><img src={image} alt={fullName} className="photo"/></div>
          <div className="desc">
            <h3 className="fullname">{fullName}</h3>
            <p className="email">{email}</p>
            <p className="phone">{phone}</p>
            <p className="street">{street}</p>
            <div>
              <span className="zipcode">{zipcode} </span>
              <span className="city">{city}</span>
            </div>
            <div className="actions">
              { !isFavorite ?
            <button className="favorite-btn" onClick={() => addFavorite(contact)}><img alt="favorize" className="icon" src={require('../images/unfavorize.png')} />Favorize</button>
            : <button className="delete unfavorize" onClick={() => removeFavorite(id)}><img alt="unfavorize" className="icon" src={require('../images/favorize.png')} />Unfavorize</button> }
              <button className="delete remove" onClick={() => removeContact(id)}>
                <img alt="delete" className="icon" src={require('../images/delete.png')} />
                Delete</button>
              <button className="btn edit" onClick={() => toggleEditMode(id)}>
                <img alt="delete" className="icon" src={require('../images/edit.png')} />
                Edit</button>
            </div>
          </div>
        </div>
    </>
  )
}

export default ContactCard;