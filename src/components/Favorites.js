import React from 'react';
import Favorite from './Favorite';

export default class Favorites extends React.Component {

  render() {
    const { favorites } = this.props;
    return (
      <>
    <h2>Favorites</h2>
      { this.props.favorites ?
    <div className="friends favorites">
    {favorites.map((favorite, index) => (
      <Favorite 
        removeFavorite={this.props.removeFavorite}
        favorite={favorite}
        key={index}
        />
    ))}
    </div>
    : <h2>Fail</h2> }
      </>
    )
  }
}