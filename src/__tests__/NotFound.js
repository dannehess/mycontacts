import React from 'react';
import { shallow } from 'enzyme';

import NotFound from '../components/NotFound';

it('Should render a h1 with NOT FOUND!', () => {
    const wrapper = shallow(<NotFound />);

    expect(wrapper.find('h1').text()).toEqual('NOT FOUND!');
})