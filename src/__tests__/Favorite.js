import React from 'react';
import Favorite from '../components/Favorites';
import { shallow, mount } from 'enzyme';

describe('Favorite', () => {

  let wrapper;
  let unFavorize;

  beforeEach(() => {
    unFavorize = jest.fn();
    wrapper = mount(<Favorite unFavorize={unFavorize}/>);
  })
  it('Renders', () => {
    expect(wrapper);
  })
  it('Adds required prop', () => {
    expect(wrapper.props().unFavorize).toBeDefined();
  })
})