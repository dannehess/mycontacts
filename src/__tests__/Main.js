import React from 'react';
import { mount, shallow } from 'enzyme';

import Main from '../components/Main';
import NewContact from '../components/NewContact';
import ContactCard from '../components/ContactCard';
import EditContact from '../components/EditContact';
import Favorites from '../components/Favorites';

let wrapper;

beforeEach(() => {
    wrapper = mount(<Main />);
});

describe('<Main /> render', () => {
    it('renders correctly', () => {
        expect(wrapper).toMatchSnapshot();
    });
})

describe('NewContact', () => {
    it('Form should toggle show/hide onclick', () => {
        const wrap = shallow(<NewContact />);
        const instance = wrap.instance();
        expect(wrap.state('showing')).toBe(false);
        instance.toggleVisibility();
        expect(wrap.state('showing')).toBe(true);
    });
})

describe('ContactCard', () => {
    it('should render 6 contacts', () => {
        const contacts = wrapper.find('.all');
    expect(contacts.find(ContactCard)).toHaveLength(6);
})
    it('EditContact-component should be hidden', () => {
        expect(wrapper.state().isInEditMode).toBe(false);
    })
    it('Should render after click', () => {
        wrapper.find('.edit').first().simulate('click');
        expect(wrapper.state().isInEditMode).toBe(true);
    })
})  

describe('Checkhighestid', () => {
    it('Should increment by 1', () => {
        const wrapper = mount(<Main />);
        const instance = wrapper.instance();
        expect(instance.checkHighestId()).toBe(6);
    })
})

describe('<Favorites/>', () => {
    it('Favorites-component should be hidden', () => {
        expect(wrapper.state().favorites.length).toBe(0); 
        expect(wrapper.find(Favorites).exists()).toBeFalsy();  
    })
    it('Favorites-component should show', () => {
        wrapper.setState({
            favorites: [{
                fullName: 'Daniel'
            }]
        })
        expect(wrapper.state().favorites.length).toBe(1); 
        wrapper.update();
        expect(wrapper.find(Favorites).exists()).toBeTruthy();  
    })
})