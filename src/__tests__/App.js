import React from 'react';
import App from '../App';
import Header from '../components/Header';
import { shallow, mount } from 'enzyme';

describe('Render <App />', () => {
    it('renders correctly', () => {
        const wrapper = mount(<App />);
        expect(wrapper).toMatchSnapshot(); 
    });
})