import React from 'react';
import { shallow, mount } from 'enzyme';
import Logo from '../components/Logo';

describe('Should render', () => {
    it('Should render', () => {
        const wrapper = shallow(<Logo />);
        expect(wrapper.find('.logo'));
        expect(wrapper.find('.logo').text()).toContain('My Contacts')
        expect(wrapper.find('.logoimg'));
    })
})