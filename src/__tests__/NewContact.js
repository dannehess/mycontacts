import React from 'react';
import NewContact from '../components/NewContact';
import { shallow, mount } from 'enzyme';

describe('<NewContact /> rendering', () => {
  let wrapper;

  beforeEach(() => {
    wrapper = shallow(<NewContact />);
  });

  it('renders correctly', () => {
    expect(wrapper).toMatchSnapshot();
  });
  it('Show form with all elements', () => {
    const addForm = wrapper.find('create-contact-forms')
    expect(addForm.find('.add-btn'));
    expect(addForm.find('.delete'));
  });
  it('clearForm Should clear state and hide form', () => {
    const instance = wrapper.instance();
    instance.clearForm();
    expect(wrapper.state('showing')).toBe(false);
  });
});

describe('NewContact methods', () => {
  it('should add', () => {
    const wrapper = mount(<NewContact/>);
    wrapper.setState({
    contact: {
      id: 7,
      image: 'https://danielhessling.com/assets/images/avatar.png',
      fullName: 'Daniel Hessling',
      email: 'daniel.hessling@gmail.com',
      phone: '07740493933',
      street: 'Fakestreet',
      zipcode: '123 45',
      city: 'Faketown'
     }
   })   
  });
});