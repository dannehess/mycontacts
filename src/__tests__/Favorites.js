import React from 'react';
import { shallow, mount } from 'enzyme';
import Favorites from '../components/Favorites';
import Favorite from '../components/Favorite';

describe('<Favorites /> render', () => {
    it('Should be hidden if there is no favorites', () => {
        const props = {
            favorites: [{fullname: "Daniel"}]
        };
        const wrapper = mount(<Favorites {...props}/>);
        expect(wrapper.contains('.friends'));
    })
})