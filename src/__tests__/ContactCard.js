import React from 'react';
import { shallow, mount } from 'enzyme';
import ContactCard from '../components/ContactCard';

describe('ContactCard', () => {
    let contactcard;
    
    beforeEach(() => {
        contactcard = shallow(<ContactCard contact={contact}/>)
    })

    const contact = {
        index: 1,
        id: 7,
        image: 'https://danielhessling.com/assets/images/avatar.png',
        fullName: 'Daniel Hessling',
        email: 'daniel.hessling@gmail.com',
        phone: '07740493933',
        street: 'Fakestreet',
        zipcode: '123 45',
        city: 'Faketown'
        }

    it('should render Daniel Hessling', () => {
      expect(contactcard.find('.photo').prop("src")).toEqual('https://danielhessling.com/assets/images/avatar.png');
      expect(contactcard.find('.fullname').text()).toEqual('Daniel Hessling');
      expect(contactcard.find('.email').text()).toEqual('daniel.hessling@gmail.com');
      expect(contactcard.find('.phone').text()).toEqual('07740493933');
      expect(contactcard.find('.street').text()).toEqual('Fakestreet');
      expect(contactcard.find('.zipcode').text()).toEqual('123 45 ');
      expect(contactcard.find('.city').text()).toEqual('Faketown');
    })
  });